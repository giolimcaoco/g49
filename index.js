/*
	placeholder API: https://jsonplaceholder.typicode.com/posts
*/

// GET data
fetch(`https://jsonplaceholder.typicode.com/posts`).then((response) => response.json()).then((data) => showPosts(data))

// SHOW data
const showPosts = (posts)=> {
	let postEntries = [ ];


	posts.forEach((post) =>{
	
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// ADD Post data
document.querySelector(`#form-add-post`).addEventListener(`submit`, (e) => {
	e.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		body: JSON.stringify({
			title: document.querySelector(`#txt-title`).value,
			body: document.querySelector(`#txt-body`).value,
			userId: 1
		}),
		headers: {'Content-type':  'application/json; charset=UTF-8'}
	})
	.then((response)=> response.json())
	.then((data) => {
		console.log(data);
		alert(`Successfully Added`);
			document.querySelector('#txt-title').value = null
			document.querySelector('#txt-body').value = null
	})
})

// EDIT Post data
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
	document.querySelector('#btn-submit-update').removeAttribute(`disabled`) //.disabled = false
}

// UPDATE Post data
document.querySelector(`#form-edit-post`).addEventListener(`submit`, (e) => {
	e.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: "PUT",
		body: JSON.stringify({
			id: document.querySelector(`#txt-edit-id`).value,
			title: document.querySelector(`#txt-edit-title`).value,
			body: document.querySelector(`#txt-edit-body`).value,
			userId: 1
		}),
		headers: {'Content-type':  'application/json; charset=UTF-8'}
	})
	.then((response)=> response.json())
	.then((data) => {
		console.log(data);
		alert(`Successfully Updated`);

			document.querySelector(`#post-title-${data.id}`).innerHTML = data.title;
			document.querySelector(`#post-body-${data.id}`).innerHTML = data.body;

			document.querySelector('#txt-edit-id').value = null;
			document.querySelector('#txt-edit-title').value = null;
			document.querySelector('#txt-edit-body').value = null;
			document.querySelector('#btn-submit-update').setAttribute(`disabled`,true) //.disabled = true
			// setAttribute is used to allow setting up or bring back the removed attribute of the HTML elements
			/*
				it is accepting 2 elements
				1 - the string that identifies the attribute to be set
				2 - boolean to state whether the attribute is to be set (true); (false) is not accepted as the removing of the attribute; but removing the true would not work for the setAttribute 
			*/
	})
})


// ------------------------ S49 Activity ------------------------
// DELETE Post data
const deletePost = (id) =>{
	//document.querySelectorAll('.someselector').forEach(e => e.remove());
	//document.querySelector(`#post-${id}`).innerHTML = null
	document.querySelector(`#post-${id}`).remove()
}
